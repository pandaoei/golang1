package main

import (
	"fmt"
	"math"

	"gitlab.com/pandaoei/go_crash_course/03_packages/strutil"
)

func main() {
	fmt.Println("Hello World")
	fmt.Println(math.Floor(2.7))
	fmt.Println(math.Ceil(2.7))
	fmt.Println(math.Sqrt(16))
	fmt.Println(strutil.Reverse("test"))
}
