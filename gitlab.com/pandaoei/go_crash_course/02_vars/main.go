package main

import "fmt"

func main() {
	// using var
	var name string = "Pandaoei"
	var age int32 = 36
	var isCool = true
	isCool = false

	//shorthand
	name2 := "Pandaoei2"
	size := 1.3
	email1, email2 := "pandaoei@test.com", "pandaoei2@testing2.com"

	fmt.Println(name, age, name2, size, email1, email2)
	fmt.Printf("%T\n", isCool)

}
