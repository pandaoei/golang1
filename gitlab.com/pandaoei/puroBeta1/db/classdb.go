package classdb

import "fmt"

// MasterItem ...
type MasterItem struct {
	ItemNo   string `json:"ItemNo"`
	ItemName string `json:"ItemName"`
	ItemUM   string `json:"ItemUM"`
}

// DbDriver ...
const DbDriver = "postgres"

// User ...
const User = "postgres"

// Password ...
const Password = "P@ssw0rd"

//DbName ...
const DbName = "postgres"

//DataSourceName ...
var DataSourceName = fmt.Sprintf("host=localhost port=5432 user=%s password=%s dbname=%s sslmode=disable", User, Password, DbName)
