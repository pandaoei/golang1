package classdb

import "fmt"

// MasterPasien ...
type MasterPasien struct {
	PasienID          string `json:"PasienID"`
	PasienName        string `json:"PasienName"`
	PasienPhoneNo     string `json:"PasienPhoneNo"`
	PasienDateofBirth string `json:"PasienDateofBirth"`
}

// DbDriver ...
const DbDriver = "mysql"

// User ...
const User = "root"

// Password ...
const Password = "admin"

//DbName ...
const DbName = "puroapps"

//DataSourceName ...
var DataSourceName = fmt.Sprintf("%s:%s@tcp(127.0.0.1:3306)/%s?charset=utf8", User, Password, DbName)
