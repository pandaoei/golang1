package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql" // $ go get -u github.com/go-sql-driver/mysql
	"github.com/gorilla/mux"
	classdb "gitlab.com/pandaoei/puromedic/db"
)

var db *sql.DB
var err error

func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World!\n"))
}

/*
func getMasterItems(w http.ResponseWriter, r *http.Request) {
	//w.Write([]byte("Hello World!\n"))
	w.Header().Set("Content-Type", "application/json")

	// //w.Write([]byte("\n==> SelectMasterItem1"))
	InitDatabase()
	// Execute the query
	results, err := db.Query("SELECT itemno, itemname, itemum FROM MasterItem")
	checkErr(err)
	var MasterItems []classdb.MasterItem

	for results.Next() {
		//var MasterItem classdb.MasterItem
		var MasterItem classdb.MasterItem

		err = results.Scan(&MasterItem.ItemNo, &MasterItem.ItemName, &MasterItem.ItemUM)
		checkErr(err)
		MasterItems = append(MasterItems, classdb.MasterItem{ItemNo: MasterItem.ItemNo, ItemName: MasterItem.ItemName, ItemUM: MasterItem.ItemUM})
	}
	json.NewEncoder(w).Encode(MasterItems)
} */

func getMasterPasienS(w http.ResponseWriter, r *http.Request) {
	//w.Write([]byte("Hello World!\n"))
	w.Header().Set("Content-Type", "application/json")

	// //w.Write([]byte("\n==> SelectMasterItem1"))
	InitDatabase()
	// Execute the query
	results, err := db.Query("SELECT PasienID, PasienName, PasienPhoneNo, PasienDateofBirth FROM masterpasien")
	checkErr(err)
	var MasterPasiens []classdb.MasterPasien

	for results.Next() {
		//var masterpasien classdb.masterpasien
		var MasterPasien classdb.MasterPasien

		err = results.Scan(&MasterPasien.PasienID, &MasterPasien.PasienName, &MasterPasien.PasienPhoneNo, &MasterPasien.PasienDateofBirth)
		checkErr(err)
		MasterPasiens = append(MasterPasiens, classdb.MasterPasien{PasienID: MasterPasien.PasienID, PasienName: MasterPasien.PasienName, PasienPhoneNo: MasterPasien.PasienPhoneNo, PasienDateofBirth: MasterPasien.PasienDateofBirth})
	}
	json.NewEncoder(w).Encode(MasterPasiens)
}

/*
func getMasterItem(w http.ResponseWriter, r *http.Request) {
	//w.Write([]byte("Hello World!\n"))
	InitDatabase()
	params := mux.Vars(r) // Get params

	var MasterItem classdb.MasterItem

	var sqlStm = fmt.Sprintf("SELECT itemno, itemname, itemum FROM MasterItem where itemno = $1")

	err = db.QueryRow(sqlStm, params["id"]).Scan(&MasterItem.ItemNo, &MasterItem.ItemName, &MasterItem.ItemUM)
	checkErr(err)

	db.Close()
	w.Header().Set("Content-Type", "application/json")
	//w.Write([]byte("\n==> SelectMasterItem1"))
	json.NewEncoder(w).Encode(MasterItem)
} */

func getMasterPasien(w http.ResponseWriter, r *http.Request) {
	//w.Write([]byte("Hello World!\n"))
	InitDatabase()
	params := mux.Vars(r) // Get params

	var MasterPasien classdb.MasterPasien

	var sqlStm = fmt.Sprintf("SELECT PasienID, PasienName, PasienPhoneNo, PasienDateofBirth FROM masterpasien where PasienID = ?")

	err = db.QueryRow(sqlStm, params["id"]).Scan(&MasterPasien.PasienID, &MasterPasien.PasienName, &MasterPasien.PasienPhoneNo, &MasterPasien.PasienDateofBirth)
	checkErr(err)

	db.Close()
	w.Header().Set("Content-Type", "application/json")
	//w.Write([]byte("\n==> SelectMasterItem1"))
	json.NewEncoder(w).Encode(MasterPasien)
}

// InitDatabase ...
func InitDatabase() {
	fmt.Printf("Opening the %s ...", classdb.DbName)
	db, err = sql.Open(classdb.DbDriver, classdb.DataSourceName)

	if err != nil {
		panic(err.Error())
	}
	//else {
	//fmt.Println("Success!")
	//}

	//defer db.Close()

}

func main() {
	//InitDatabase()
	r := mux.NewRouter()

	// Routes consist of a path and a handler function.
	r.HandleFunc("/", index)
	r.HandleFunc("/api/MasterPasien", getMasterPasienS).Methods("GET")
	r.HandleFunc("/api/MasterPasien/{id}", getMasterPasien).Methods("GET")
	fmt.Println("Server Starting at port 8000...")
	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(":8000", r))
	//SelectMasterItem1()
	//SelectMasterItem2()
}

// func SelectMasterItem1() {
// 	fmt.Println("\n==> SelectMasterItem1")

// 	// Execute the query
// 	results, err := db.Query("SELECT itemno, itemname, itemum FROM MasterItem")
// 	checkErr(err)

// 	for results.Next() {
// 		var MasterItem classdb.MasterItem

// 		err = results.Scan(&MasterItem.ItemNo, &MasterItem.ItemName, &MasterItem.ItemUM)
// 		checkErr(err)

// 		fmt.Printf("%s\t%s\t%s \n", MasterItem.ItemNo, MasterItem.ItemName, MasterItem.ItemUM)
// 	}
// }

// func SelectMasterItem2() {
// 	fmt.Println("\n==> SelectMasterItem2")
// 	var MasterItem classdb.MasterItem

// 	var sqlStm = fmt.Sprintf("SELECT itemno, itemname, itemum FROM MasterItem where itemno = $1")

// 	err = db.QueryRow(sqlStm, "Item002").Scan(&MasterItem.ItemNo, &MasterItem.ItemName, &MasterItem.ItemUM)
// 	checkErr(err)

// 	fmt.Printf("%s\t%s\t%s \n", MasterItem.ItemNo, MasterItem.ItemName, MasterItem.ItemUM)

// }

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
